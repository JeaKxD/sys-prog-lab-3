﻿using System;
using System.IO;
using System.Management;
using System.Security.Permissions;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Lab3 : Form
    {
        public Lab3() => InitializeComponent();

        public void getLogicalDriversAndInfo()
        {
            Discs.Items.Clear();
            foreach (var drive in DriveInfo.GetDrives())
            {
                try
                {
                    Discs.Items.Add($"Ім'я диску: { drive.Name }");
                    Discs.Items.Add($"Файлова система: { drive.DriveFormat }");
                    Discs.Items.Add($"Тип диска: { drive.DriveType }");
                    Discs.Items.Add($"Вільне місце на диску: { Math.Round(drive.TotalFreeSpace / Math.Pow(1024, 3), 1) } ГБ");
                    Discs.Items.Add($"Розмір диска: { Math.Round(drive.TotalSize / Math.Pow(1024, 3), 1) } ГБ");
                }
                catch { }

                Discs.Items.Add("\n");
            }
        }

        public void Operativa()
        {
            SystemMemory.Items.Clear();
            var connection = new ConnectionOptions() { Impersonation = ImpersonationLevel.Impersonate };
            var scope = new ManagementScope("\\root\\CIMV2", connection);
            var query = new ObjectQuery("SELECT * FROM Win32_PhysicalMemory");

            scope.Connect();

            var searcher = new ManagementObjectSearcher(scope, query);

            foreach (ManagementObject queryObj in searcher.Get())
            {
                SystemMemory.Items.Add($"Name:{queryObj["Name"]}");
                SystemMemory.Items.Add($"Status:{queryObj["Status"]}");
                SystemMemory.Items.Add($"Capacity:{Convert.ToInt64(queryObj["Capacity"].ToString()) / Math.Pow(1024, 3)} ГБ");
                SystemMemory.Items.Add($"SerialNumber:{queryObj["SerialNumber"]}");
                SystemMemory.Items.Add($"Speed:{queryObj["Speed"]}");
            }
        }

        public void SystemInformationInfo()
        {
            ComputerInfo.Items.Clear();
            ManagementObjectSearcher searcher5 = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_OperatingSystem");

            foreach (ManagementObject queryObj in searcher5.Get())
            {
                ComputerInfo.Items.Add($"PC Name: {System.Net.Dns.GetHostName()}");
                ComputerInfo.Items.Add($"BuildNumber: {queryObj["BuildNumber"]}");
                ComputerInfo.Items.Add($"ОС: {queryObj["Caption"]}");
                ComputerInfo.Items.Add($"SerialNumber: {queryObj["SerialNumber"]}");
                ComputerInfo.Items.Add($"SystemDirectory: {queryObj["SystemDirectory"]}");
                ComputerInfo.Items.Add($"SystemDrive: {queryObj["SystemDrive"]}");
                ComputerInfo.Items.Add($"ОС Version: {queryObj["Version"]}");
            }
        }

        public void SystemDirectoryInfo()
        {
            SystemCatalog.Items.Clear();
            string directory = @"C:\Windows";
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            SystemCatalog.Items.Add($"Назва каталога: {dirInfo.Name}");
            SystemCatalog.Items.Add($"Повна назва каталога: {dirInfo.FullName}");
            SystemCatalog.Items.Add($"Час створення каталога: {dirInfo.CreationTime}");
            SystemCatalog.Items.Add($"Кореневий каталог: {dirInfo.Root}");

            string directory2 = @"C:\\Windows\\Temp";
            DirectoryInfo dirInfo2 = new DirectoryInfo(directory2);
            SystemCatalog.Items.Add($"Назва каталога: {dirInfo2.Name}");
            SystemCatalog.Items.Add($"Повна назва каталога: {dirInfo2.FullName}");
            SystemCatalog.Items.Add($"Час створення каталога: {dirInfo2.CreationTime}");
            SystemCatalog.Items.Add($"Кореневий каталог: {dirInfo2.Root}");

            string directory3 = AppDomain.CurrentDomain.BaseDirectory;
            DirectoryInfo dirInfo3 = new DirectoryInfo(directory3);
            SystemCatalog.Items.Add($"Назва каталога: {dirInfo3.Name}");
            SystemCatalog.Items.Add($"Повна назва каталога: {dirInfo3.FullName}");
            SystemCatalog.Items.Add($"Час створення каталога: {dirInfo3.CreationTime}");
            SystemCatalog.Items.Add($"Кореневий каталог: {dirInfo3.Root}");
        }

        private void GetAllSystemInfoButton_Click(object sender, EventArgs e)
        {
            GetAllSystemInfoButton.Text = "Refresh";
            getLogicalDriversAndInfo();
            Operativa();
            SystemInformationInfo();
            SystemDirectoryInfo();

            try { StartWatching(); }
            catch (Exception) { MessageBox.Show("Choose directory!"); }
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void StartWatching()
        {
            FileSystemWatcher fileWatcher = new FileSystemWatcher();
            fileWatcher.Path = path;
            fileWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.LastWrite;
            fileWatcher.Filter = "*.*";
            fileWatcher.Created += new FileSystemEventHandler(OnCreated);
            fileWatcher.Changed += new FileSystemEventHandler(OnChanged);
            fileWatcher.Deleted += new FileSystemEventHandler(OnDelete);
            fileWatcher.Renamed += new RenamedEventHandler(OnRanamed);
            fileWatcher.Error += new ErrorEventHandler(OnError);
            fileWatcher.EnableRaisingEvents = true;
        }

        private async void OnCreated(object sender, FileSystemEventArgs e) => CheckCatalogLogs.Invoke(new Action(() => CheckCatalogLogs.Items.Add("File: " + e.Name + " Do: " + e.ChangeType + " Directory: " + e.FullPath)));
        private async void OnChanged(object sender, FileSystemEventArgs e) => CheckCatalogLogs.Invoke(new Action(() => CheckCatalogLogs.Items.Add("File change: " + e.Name + " Do: " + e.ChangeType + " Directory: " + e.FullPath)));
        private async void OnDelete(object sender, FileSystemEventArgs e) => CheckCatalogLogs.Invoke(new Action(() => CheckCatalogLogs.Items.Add("File delete: " + e.Name + " Do: " + e.ChangeType + " Delete from" + e.FullPath)));
        private async void OnRanamed(object sender, RenamedEventArgs e) => CheckCatalogLogs.Invoke(new Action(() => CheckCatalogLogs.Items.Add($"File: {e.OldFullPath}  rename to {e.FullPath}")));
        private async void OnError(object sender, ErrorEventArgs e) => CheckCatalogLogs.Invoke(new Action(() => CheckCatalogLogs.Items.Add("Error")));

        string path;

        private void SetCheckCatalog_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    path = dialog.SelectedPath;
                    label7.Text = "Directory: " + path;
                }
        }
    }
}