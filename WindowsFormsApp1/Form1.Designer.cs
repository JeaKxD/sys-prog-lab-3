﻿namespace WindowsFormsApp1
{
    partial class Lab3
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComputerInfo = new System.Windows.Forms.ListBox();
            this.GetAllSystemInfoButton = new System.Windows.Forms.Button();
            this.Discs = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SystemMemory = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SystemCatalog = new System.Windows.Forms.ListBox();
            this.CheckCatalogLogs = new System.Windows.Forms.ListBox();
            this.SetCheckCatalogButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ComputerInfo
            // 
            this.ComputerInfo.FormattingEnabled = true;
            this.ComputerInfo.HorizontalScrollbar = true;
            this.ComputerInfo.Location = new System.Drawing.Point(391, 249);
            this.ComputerInfo.Name = "ComputerInfo";
            this.ComputerInfo.Size = new System.Drawing.Size(366, 212);
            this.ComputerInfo.TabIndex = 0;
            // 
            // GetAllSystemInfoButton
            // 
            this.GetAllSystemInfoButton.BackColor = System.Drawing.Color.Transparent;
            this.GetAllSystemInfoButton.Location = new System.Drawing.Point(646, 745);
            this.GetAllSystemInfoButton.Margin = new System.Windows.Forms.Padding(0);
            this.GetAllSystemInfoButton.Name = "GetAllSystemInfoButton";
            this.GetAllSystemInfoButton.Size = new System.Drawing.Size(112, 32);
            this.GetAllSystemInfoButton.TabIndex = 1;
            this.GetAllSystemInfoButton.Text = "Start";
            this.GetAllSystemInfoButton.UseVisualStyleBackColor = false;
            this.GetAllSystemInfoButton.Click += new System.EventHandler(this.GetAllSystemInfoButton_Click);
            // 
            // Discs
            // 
            this.Discs.FormattingEnabled = true;
            this.Discs.HorizontalScrollbar = true;
            this.Discs.Location = new System.Drawing.Point(11, 35);
            this.Discs.Name = "Discs";
            this.Discs.Size = new System.Drawing.Size(367, 173);
            this.Discs.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(177, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Disk";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(517, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "System memory";
            // 
            // SystemMemory
            // 
            this.SystemMemory.FormattingEnabled = true;
            this.SystemMemory.HorizontalScrollbar = true;
            this.SystemMemory.Location = new System.Drawing.Point(391, 35);
            this.SystemMemory.Name = "SystemMemory";
            this.SystemMemory.Size = new System.Drawing.Size(366, 173);
            this.SystemMemory.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(478, 222);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Computer, system info";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(28, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(311, 24);
            this.label4.TabIndex = 7;
            this.label4.Text = "System, temporary, current directory";
            // 
            // SystemCatalog
            // 
            this.SystemCatalog.FormattingEnabled = true;
            this.SystemCatalog.HorizontalScrollbar = true;
            this.SystemCatalog.Location = new System.Drawing.Point(11, 249);
            this.SystemCatalog.Name = "SystemCatalog";
            this.SystemCatalog.Size = new System.Drawing.Size(367, 212);
            this.SystemCatalog.TabIndex = 9;
            // 
            // CheckCatalogLogs
            // 
            this.CheckCatalogLogs.FormattingEnabled = true;
            this.CheckCatalogLogs.HorizontalScrollbar = true;
            this.CheckCatalogLogs.Location = new System.Drawing.Point(10, 501);
            this.CheckCatalogLogs.Name = "CheckCatalogLogs";
            this.CheckCatalogLogs.ScrollAlwaysVisible = true;
            this.CheckCatalogLogs.Size = new System.Drawing.Size(747, 238);
            this.CheckCatalogLogs.TabIndex = 10;
            // 
            // SetCheckCatalogButton
            // 
            this.SetCheckCatalogButton.BackColor = System.Drawing.Color.Transparent;
            this.SetCheckCatalogButton.Location = new System.Drawing.Point(12, 745);
            this.SetCheckCatalogButton.Name = "SetCheckCatalogButton";
            this.SetCheckCatalogButton.Size = new System.Drawing.Size(112, 32);
            this.SetCheckCatalogButton.TabIndex = 16;
            this.SetCheckCatalogButton.Text = "Choose directory";
            this.SetCheckCatalogButton.UseVisualStyleBackColor = false;
            this.SetCheckCatalogButton.Click += new System.EventHandler(this.SetCheckCatalog_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(8, 478);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 20);
            this.label7.TabIndex = 17;
            this.label7.Text = "Directory";
            // 
            // Lab3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(767, 782);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.SetCheckCatalogButton);
            this.Controls.Add(this.CheckCatalogLogs);
            this.Controls.Add(this.SystemCatalog);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SystemMemory);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Discs);
            this.Controls.Add(this.GetAllSystemInfoButton);
            this.Controls.Add(this.ComputerInfo);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(783, 820);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(783, 820);
            this.Name = "Lab3";
            this.Text = "Lab3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private System.Windows.Forms.Button GetAllSystemInfoButton;
        private System.Windows.Forms.Button SetCheckCatalogButton;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox ComputerInfo;
        private System.Windows.Forms.ListBox Discs;
        private System.Windows.Forms.ListBox SystemMemory;
        private System.Windows.Forms.ListBox SystemCatalog;
        private System.Windows.Forms.ListBox CheckCatalogLogs;

        #endregion

        private System.Windows.Forms.ListBox listBox5;
    }
}

